//
//  APIResponse.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ApiResponse {
    var success: Bool
    var message: String
    var data: JSON?
    
    init(json: JSON) {
        success = json[""].stringValue == "success" ? true : false
        message = json["message"].stringValue
        data = json
    }
}
