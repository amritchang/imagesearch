//
//  ImagesVideo.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ImageVideo {
    var id: String
    var url: String
    var isVideo: Bool
    var large_url: String
    var source_id: String
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.url = json["url"].stringValue
        self.large_url = json["large_url"].stringValue
        self.isVideo = (json["large_url"].stringValue.contains(".mp4"))
        self.source_id = json["source_id"].stringValue
    }
}

struct ImageVideoDetail{
    var id: String
    var url: String
    var name: String
    var images: [ImageVideo]
    var image_count: String
    
    init(json: JSON) {
        self.id = json["id"].stringValue
        self.url = json["url"].stringValue
        self.name = json["name"].stringValue
        self.image_count = json["image_count"].stringValue
        self.images = json["images"].arrayValue.map{ImageVideo(json: $0)}
    }
}
