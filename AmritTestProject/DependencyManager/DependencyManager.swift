//
//  DependencyManager.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

extension SwinjectStoryboard{
    public static func setup() {
        
        //Image Video List
        defaultContainer.register(ImageVideoListViewModel.self) { resolver  in ImageVideoListViewModelImplementation()
        }
        defaultContainer.storyboardInitCompleted(ImageVideoListViewController.self) { (resolver, controller) in
            controller.imageVideoListViewModel = resolver.resolve(ImageVideoListViewModel.self)!
            controller.viewModel = resolver.resolve(ImageVideoListViewModel.self)!
        }
        
        //Image Detail
        defaultContainer.register(ImageDetailViewModel.self) { resolver, id  in
            ImageDetailViewModelModelImplementation(id: id)
        }
    }
}

