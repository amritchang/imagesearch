//
//  Global.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import UIKit
import SwinjectStoryboard

struct Global {
    static var baseUrl:String{
        return "http://www.splashbase.co/api/v1/" 
    }
}

let MainStoryboard = SwinjectStoryboard.create(name: "Main", bundle: nil)

