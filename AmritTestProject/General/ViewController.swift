//
//  ViewController.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import SCLAlertView
import M13ProgressSuite
import Alamofire
import AVKit
import AVFoundation


class ViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    let indicatorVIew = M13ProgressViewRing()
        
    
    var isLoading: Bool = false{
        didSet{
            if isLoading{
                self.presentLoader()
            }else{
                self.dismissLoader()
            }
        }
    }
    
    var viewModel: ViewModel?{
        didSet{
            self.bindToViewModel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Color.background
        self.hideKeyboardWhenTappedAround()
    }
    
    
    func bindToViewModel(){
        if viewModel == nil{
            return
        }
        
        viewModel!.isLoading.asObservable().subscribe(onNext: { (value) in
            if value {
                self.view.endEditing(true)
                self.presentLoader()
            }else{
                self.dismissLoader()
            }
        }).disposed(by: disposeBag)
        
        viewModel!.successAlert.subscribe(onNext: { (msg) in
            _ = SCLAlertView().showSuccess("Congratulations", subTitle: msg)
        }).disposed(by: disposeBag)
        
        
        viewModel!.failureAlert.subscribe(onNext: { (msg) in
            _ = SCLAlertView().showError("Sorry", subTitle: msg)
        }).disposed(by: disposeBag)
        
    }
    
    func playVideo(url: URL) {
        let player = AVPlayer(url: url)
        
        let vc = AVPlayerViewController()
        vc.player = player
        
        self.present(vc, animated: true) { vc.player?.play() }
    }
}

//Mark:- Loading
extension ViewController{
    func presentLoader(){
        //Loading.shared.startLoading()
        let background = UIView()
        background.tag = 99
        background.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        
        indicatorVIew.showPercentage = false
        indicatorVIew.progressRingWidth = 5
        indicatorVIew.backgroundRingWidth = 5
        indicatorVIew.secondaryColor = Color.primary
        indicatorVIew.indeterminate = true
        background.addSubview(indicatorVIew)
        indicatorVIew.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.height.equalTo(75)
            make.width.equalTo(75)
        }
        let app  = UIApplication.shared.delegate as! AppDelegate
        app.window!.rootViewController!.view.addSubview(background)
        
        background.snp.makeConstraints { (make) in
            make.width.equalTo(UIScreen.main.bounds.size.width)
            make.height.equalTo(UIScreen.main.bounds.size.height)
            make.center.equalToSuperview()
        }
        
    }
    
    func dismissLoader(){
        let app  = UIApplication.shared.delegate as! AppDelegate
        self.indicatorVIew.layer.removeAllAnimations()
        for item in app.window!.rootViewController!.view.subviews{
            if item.tag == 99 {
                item.removeFromSuperview()
            }
        }
    }
}



