//
//  ImageView.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage


class ImageView: UIView {
    var image: String = ""{
        didSet{
            self.layoutSubviews()
        }
    }
    
    var playVideo: (()->())?
    var isPlayBtn: Bool = false
    
    
    var playButton = UIButton()
    
    var imageView: UIImageView = UIImageView()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.addSubview(imageView)
        
        let indicator = UIActivityIndicatorView()
        indicator.color = Color.primary
        self.addSubview(indicator)
        self.bringSubviewToFront(indicator)
        indicator.snp.makeConstraints { (make) in
            make.height.equalTo(20)
            make.width.equalTo(20)
            make.center.equalToSuperview()
        }
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
            print(image)
        let url = URL(string: image.replacingOccurrences(of: " ", with: ""))
        indicator.isHidden = false
        indicator.startAnimating()
        imageView.sd_setImage(with: url, placeholderImage:  UIImage(named: "noImg"), options: .refreshCached, progress: nil) { (image, error, cache, url) in
                indicator.stopAnimating()
                if error != nil{
                    self.imageView.image = UIImage(named: "brokenImg")
                }else{
                    self.imageView.image = image
                }
        }
        imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.addSubview(playButton)
        playButton.setImage(UIImage(named: "play"), for: .normal)
        playButton.tintColor = .white
        playButton.isHidden = !isPlayBtn
        playButton.addTarget(self, action: #selector(self.playBtnPressed), for: .touchUpInside)
        playButton.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.width.equalTo(30)
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
}

extension ImageView{
    @objc func playBtnPressed(){
        self.playVideo?()
    }
}
