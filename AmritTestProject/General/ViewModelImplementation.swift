//
//  ViewModelImplementation.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON
import UIKit

class ViewModelImplementation: ViewModel {
    var isLoading: PublishSubject<Bool> = PublishSubject()
    var successAlert: PublishSubject<String> = PublishSubject()
    var failureAlert: PublishSubject<String> = PublishSubject()
    var hideEmptyLabel: Variable<Bool> = Variable(true)
    let disposeBag = DisposeBag()
    
    init(){
        
    }
}

