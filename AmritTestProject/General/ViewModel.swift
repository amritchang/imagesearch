//
//  ViewModel.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import RxSwift

protocol ViewModel{
    var isLoading: PublishSubject<Bool> { get }
    var successAlert: PublishSubject<String> { get }
    var failureAlert: PublishSubject<String> { get }
    var hideEmptyLabel: Variable<Bool> { get }
}
