//
//  ImageVideoDetailViewController.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import UIKit
import ImageSlideshow

class ImageDetailViewController: ViewController {

    var imageDetailViewModel: ImageDetailViewModel!
    var images: [InputSource] = []

    @IBOutlet weak var slideshow: ImageSlideshow!
    
    @IBOutlet weak var nameLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageDetailViewModel.fetch.onNext(())
        
        let emptyLabel = UILabel()
        emptyLabel.numberOfLines = 0
        imageDetailViewModel.emptyLblText.asObservable().bind(to: emptyLabel.rx.text).disposed(by: disposeBag)
        emptyLabel.textColor = UIColor.lightGray
        emptyLabel.textAlignment = .center
        emptyLabel.font = UIFont(name: FontStyle.Regular, size: 15.0)
        view.addSubview(emptyLabel)
        view.bringSubviewToFront(emptyLabel)
        emptyLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(15)
            make.trailing.equalTo(-15)
            make.centerY.equalToSuperview()
        }
        
        self.imageDetailViewModel.dataSource.asObservable().unwrap().subscribe(onNext: { (detail) in
            for each in detail.images{
                if let alamofireSource = AlamofireSource(urlString: each.url) {
                    self.images.append(alamofireSource)
                }
            }
            self.nameLbl.text = detail.name.uppercased()
            self.title = (detail.name == "") ? "" : "\(detail.name)'s Detail"
            self.setUpSlideShow()
        }).disposed(by: disposeBag)
        

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setUpSlideShow(){
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = Color.primary
        pageIndicator.pageIndicatorTintColor = UIColor.lightGray
        slideshow.pageIndicator = pageIndicator
        slideshow.contentScaleMode = .scaleAspectFill
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        slideshow.addGestureRecognizer(gestureRecognizer)
        self.slideshow.setImageInputs(images)
    }
    
    @objc func didTap() {
        slideshow.presentFullScreenController(from: self)
    }
    
}
