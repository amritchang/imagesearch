//
//  ImageDetailViewModel.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import RxSwift

protocol ImageDetailViewModel: ViewModel {
    var fetch: PublishSubject<Void> {get}
    var dataSource: Variable<ImageVideoDetail?> {get}
    var emptyLblText: Variable<String> {get}
}
