//
//  ImageDetailViewModelImplementation.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt
import SwiftyJSON
import SwinjectStoryboard

class ImageDetailViewModelModelImplementation: ViewModelImplementation, ImageDetailViewModel{
    var fetch: PublishSubject<Void> = PublishSubject()
    var dataSource: Variable<ImageVideoDetail?> = Variable(nil)
    var emptyLblText: Variable<String> = Variable("")
    
    init(id: String) {
        super.init()
        
        let fetchRequest = fetch
            .do(onNext: { (_) in
                self.isLoading.onNext(true)
            })
            .flatMapLatest {page in
                return APIService.sharedAPIService.request(route: .getDetail(id: id)).asObservable().materialize()
            }.do(onNext: { (_) in
                self.isLoading.onNext(false)
            }).share()
        
        fetchRequest.elements().subscribe(onNext: { (response) in
            let result =  ApiResponse(json: JSON(response))
            guard let data = result.data else { return }
            self.emptyLblText.value = result.message
            self.dataSource.value = ImageVideoDetail(json: data)
        }).disposed(by: disposeBag)
        
        fetchRequest.errors().subscribe(onNext: { (error) in
            self.isLoading.onNext(false)
            self.failureAlert.onNext(error.localizedDescription)
        }).disposed(by: disposeBag)
        
    }
}
