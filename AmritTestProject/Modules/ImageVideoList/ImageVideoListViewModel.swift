//
//  ImageVideoListViewModel.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import RxSwift

protocol ImageVideoListViewModel: ViewModel {
    var fetch: PublishSubject<Void> {get}
    var searchText: Variable<String> { get }
    var dataSource: Variable<[ImageVideo]> {get}
    var fetchAjax: PublishSubject<String> {get}
    var didSelectCell: PublishSubject<IndexPath> { get }
    var imageDetailViewModel: PublishSubject<ImageDetailViewModel> {get}
}

