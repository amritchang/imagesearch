//
//  ImageVideoListViewController.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import UIKit

class ImageVideoListViewController: ViewController, UICollectionViewDelegateFlowLayout {

    var imageVideoListViewModel: ImageVideoListViewModel!
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!{
        didSet{
            self.collectionView.register(UINib(nibName: "ImageVideoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchBar.text = ""
        
        let emptyLabel = UILabel()
        emptyLabel.numberOfLines = 0
        emptyLabel.text = ""
        emptyLabel.textColor = UIColor.lightGray
        emptyLabel.textAlignment = .center
        emptyLabel.font = UIFont(name: FontStyle.Regular, size: 15.0)
        view.addSubview(emptyLabel)
        view.bringSubviewToFront(emptyLabel)
        emptyLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(15)
            make.trailing.equalTo(-15)
            make.centerY.equalToSuperview()
        }
        
        imageVideoListViewModel.hideEmptyLabel.asObservable().subscribe(onNext: { (value) in
            emptyLabel.text = !value ? "Nothing to show." : ""
            emptyLabel.isHidden = !value ? false : true
        }).disposed(by: disposeBag)
        
        let refreshControl = UIRefreshControl()
        refreshControl.rx.controlEvent(.valueChanged).bind(to: imageVideoListViewModel.fetch).disposed(by: disposeBag)
        refreshControl.tintColor = Color.primary
        collectionView.addSubview(refreshControl)
        
        
        
        collectionView.rx.itemSelected.bind(to: imageVideoListViewModel.didSelectCell).disposed(by: disposeBag)
        
        
        self.collectionView.rx.setDelegate(self).disposed(by: disposeBag)
        
        self.searchBar.rx.text.orEmpty.bind(to: imageVideoListViewModel.searchText).disposed(by: disposeBag)
        
        imageVideoListViewModel.dataSource.asObservable()
            .do(onNext: { _ in
                refreshControl.endRefreshing()
            }).bind(to:self.collectionView.rx.items(cellIdentifier: "cell", cellType: ImageVideoCollectionViewCell.self)) { index, data, cell in
                cell.imageVideo = data
                cell.play = { value in
                    print(value)
                    if let url = NSURL(string: value){
                        self.playVideo(url: url as URL)
                    }
                }
            }.disposed(by: disposeBag)
        
        imageVideoListViewModel.imageDetailViewModel.asObservable().subscribe(onNext: { (vm) in
            let vc = MainStoryboard.instantiateViewController(withIdentifier: "ImageDetailViewController") as! ImageDetailViewController
            vc.viewModel = vm
            vc.imageDetailViewModel = vm
            self.navigationController?.pushViewController(vc, animated: true)
        }).disposed(by: disposeBag)
    
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Search"
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    @objc func refresh(){
        self.imageVideoListViewModel.fetch.onNext(())
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let cellWidth = (width - 10) / 2
        return CGSize(width: cellWidth, height: 185)
    }
}

