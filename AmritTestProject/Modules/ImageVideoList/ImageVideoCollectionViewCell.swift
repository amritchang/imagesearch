//
//  ImageVideoCollectionViewCell.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import UIKit

class ImageVideoCollectionViewCell: UICollectionViewCell {
    var imageVideo: ImageVideo?{
        didSet{
            self.icon.image = imageVideo?.url ?? ""
            self.icon.isPlayBtn = imageVideo?.isVideo ??  false
            self.icon.playVideo = {
                self.play?((self.imageVideo?.large_url) ?? "")
            }
        }
    }
    
    @IBOutlet weak var icon: ImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var play: ((String)->())?

}
