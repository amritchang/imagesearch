//
//  ImageVideoListViewModelImplementation.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation
import RxSwift
import RxSwiftExt
import SwiftyJSON
import SwinjectStoryboard

class ImageVideoListViewModelImplementation: ViewModelImplementation, ImageVideoListViewModel{
    var fetch: PublishSubject<Void> = PublishSubject()
    var searchText: Variable<String> = Variable("")
    var dataSource: Variable<[ImageVideo]> = Variable([])
    var fetchAjax: PublishSubject<String> = PublishSubject()
    var didSelectCell: PublishSubject<IndexPath> = PublishSubject()
    var imageDetailViewModel: PublishSubject<ImageDetailViewModel> = PublishSubject()
    
    override init() {
        super.init()
        let fetchRequest = fetch
            .do(onNext: { (_) in
                self.isLoading.onNext(true)
            })
            .flatMapLatest {page in
                return APIService.sharedAPIService.request(route: ApiRoute.getLatestTen).asObservable().materialize()
            }.do(onNext: { (_) in
                self.isLoading.onNext(false)
            }).share()
        
        fetchRequest.elements().subscribe(onNext: { (response) in
            let result =  ApiResponse(json: JSON(response))
            guard let data = result.data else { return }
            let images = data["images"].arrayValue
            if images.count > 0{
                self.hideEmptyLabel.value = true
                self.dataSource.value = images.map{ImageVideo(json: $0)}
            }else{
                self.hideEmptyLabel.value = false
            }
        }).disposed(by: disposeBag)
        
        fetchRequest.errors().subscribe(onNext: { (error) in
            self.isLoading.onNext(false)
            self.failureAlert.onNext(error.localizedDescription)
        }).disposed(by: disposeBag)
        
        
        searchText.asObservable().subscribe(onNext: { (search) in
            if search == "" || search.count < 1{
                self.fetch.onNext(())
            }else{
                self.fetchAjax.onNext((search))
            }
        }).disposed(by: disposeBag)
        
        
        let fetchAjaxRequest = fetchAjax
            .do(onNext: { (_) in
                self.isLoading.onNext(false)
            }).throttle(0.5, scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMapLatest { (search) in
                return APIService.sharedAPIService.request(route: ApiRoute.getSearchList(search: search)).asObservable().materialize()
            }.do(onNext: { _ in
                self.isLoading.onNext(false)
            }).share()
        
        fetchAjaxRequest.elements().subscribe(onNext: { (response) in
            self.dataSource.value = []
            let result =  ApiResponse(json: JSON(response))
            guard let data = result.data else { return }
            let images = data["images"].arrayValue
            if images.count > 0{
                self.hideEmptyLabel.value = true
                self.dataSource.value = images.map{ImageVideo(json: $0)}
            }else{
                self.hideEmptyLabel.value = false
            }
        }).disposed(by: disposeBag)
        
        fetchAjaxRequest.errors().subscribe(onNext: { (error) in
            self.isLoading.onNext(false)
            self.failureAlert.onNext(error.localizedDescription)
        }).disposed(by: disposeBag)
        
        didSelectCell.asObservable().subscribe(onNext: { (index) in
            if !self.dataSource.value[index.row].isVideo{
                if let vm = SwinjectStoryboard.defaultContainer.resolve(ImageDetailViewModel.self, argument: self.dataSource.value[index.row].id){
                    self.imageDetailViewModel.onNext(vm)
                }
            }
        }).disposed(by: disposeBag)
        
    }
}
