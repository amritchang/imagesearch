//
//  APIManager.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiRoute {
    case getLatestTen
    case getSearchList(search: String)
    case getDetail(id: String)
}

extension ApiRoute {
    var baseURL: URL { return URL(string: Global.baseUrl)! }
    
    var path: String {
        switch self {
        case .getLatestTen:
            return "images/latest"
        case .getSearchList(let search):
            return "images/search?query=\(search)"
        case .getDetail(let id):
            return "sources/\(id)"
        }
    }
    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    var parameters: [String: Any]? {
        switch self {
        default:
            return nil
        }
    }
    var headers: [String: String]? {
        switch self {
        default:
            return ["Content-type": "application/json"]
        }
        
    }
}



private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}

