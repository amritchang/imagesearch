//
//  APIService.swift
//  AmritTestProject
//
//  Created by Amrit on 10/09/19.
//  Copyright © 2019 Amrit. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RxSwift
import RxSwiftExt


class APIService: NSObject {
    class var sharedAPIService: APIService {
        struct Singleton {
            static let sharedInstance: APIService = APIService()
        }
        return Singleton.sharedInstance
    }
    
    func request(route :ApiRoute)->Observable<Any>{
        return Observable.create({ (observer) -> Disposable in
            let url = "\(route.baseURL)\(route.path)"
            debugPrint(url)
            debugPrint(route.headers ?? [])
            debugPrint(route.parameters ?? [])
            debugPrint(route.method)
            Alamofire.request(url, method: route.method, parameters: route.parameters, encoding: JSONEncoding.default, headers: route.headers)
                .responseJSON(completionHandler: { (response) in
                    switch response.result{
                    case .success:
                        if let res = response.result.value{
                            observer.onNext(res)
                        }
                        break
                    case.failure(let error):
                        observer.onError(error)
                        break
                    }
                })
            return Disposables.create()
        })
    }
}

